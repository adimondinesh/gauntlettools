﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : ScriptableObject
{
    public Texture projTex;    // Texture for Projectile
    public string projName;    // Name of Projectile 
    public int projDamage;     // Damage of Projectile
    public int projSpeed;      // Speed of Projectile

    public Projectile(Texture tex, string name, int damage, int speed)
    {
        projTex = tex;
        projName = name;
        projDamage = damage;
        projSpeed = speed;
    }
}
