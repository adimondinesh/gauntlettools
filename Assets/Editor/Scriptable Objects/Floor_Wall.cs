﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor_Wall : ScriptableObject
{
    public Texture f_w_Tex;             // Floor or Wall Sprite
    public string f_w_Name;             // Floor or Wall Name
    public int typeIndex = 0;           // the type either floor or wall
    public bool collidable = false;     // Check if Floor or Wall is collidable 
    public bool destructible = false;   // Check if Floor or Wall is destructible

    public Floor_Wall(Texture tex, string name, int type, bool collide, bool destruct)
    {
        f_w_Tex = tex;
        f_w_Name = name;
        typeIndex = type;
        collidable = collide;
        destructible = destruct;
    }
}
