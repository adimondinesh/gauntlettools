﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : ScriptableObject
{
    public Texture enemySpawnerTex1;    // Enemy Spawner sprite for stage 1 (Initial)
    public Texture enemySpawnerTex2;    // Enemy Spawner sprite for stage 2 (Weak)
    public Texture enemySpawnerTex3;    // Enemy Spawner sprite for stage 3 (Feeble)
    public string enemySpawnerName;            // Spawner Name
    public int spawnerHealth = 0;              // Spawner Health
    public int spawnInterval = 0;              // Spawn Interval

    public Texture enemyTex;            // Enemy sprite
    public string enemyName;                   // Enemy Name
    public int enemyHealth = 0;                // Enemy Health
    public int enemyDamage = 0;                // Enemy Damage
    public string enemyWeaponName;             // Enemy weapon name
    public int enemyWeaponDamage = 0;          // Enemy weapon damage
    public int enemySpeed = 0;

    public EnemySpawner(Texture spawnerTex1, Texture spawnerTex2, Texture spawnerTex3, string spawnerName, int sHealth, int sInterval, Texture eTex, string eName, int eHealth, int eDamage, string eWeaponName, int eWeaponDamage, int eSpeed)
    {
        enemySpawnerTex1 = spawnerTex1;
        enemySpawnerTex2 = spawnerTex2;
        enemySpawnerTex3 = spawnerTex3;
        enemySpawnerName = spawnerName;
        spawnerHealth = sHealth;
        spawnInterval = sInterval;
        enemyTex = eTex;
        enemyName = eName;
        enemyHealth = eHealth;
        enemyDamage = eDamage;
        enemyWeaponName = eWeaponName;
        enemyWeaponDamage = eWeaponDamage;
        enemySpeed = eSpeed;
    }
}
