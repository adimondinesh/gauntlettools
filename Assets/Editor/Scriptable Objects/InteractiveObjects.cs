﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObjects : ScriptableObject
{
    public Texture ioTex;        // Interactive Obj sprite
    string io_Name;              // Interactive Obj name    
    bool ioCollidable = false;   // is it collidable
    bool ioDestructible = false; // is it destructible

    public InteractiveObjects(Texture tex, string name, bool collide, bool destruct)
    {
        ioTex = tex;
        io_Name = name;
        ioCollidable = collide;
        ioDestructible = destruct;
    }
}
