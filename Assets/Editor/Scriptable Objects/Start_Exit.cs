﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Start_Exit : ScriptableObject
{
    public Texture startTex;    // Start point sprite
    public Texture exitTex;     // End point sprite 

    public Start_Exit(Texture start, Texture exit)
    {
        startTex = start;
        exitTex = exit;
    }
}
