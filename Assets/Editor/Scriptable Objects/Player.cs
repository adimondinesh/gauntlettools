﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : ScriptableObject
{
    public Texture playerTex;   // player sprite
    public string playerName;          // player name
    public int playerHealth = 0;       // player health
    public string weaponName;          // player weapon name
    public int weaponDamage = 0;       // player weapon damage
    public int playerSpeed = 5;

    public Player()
    {

    }
    public Player(Texture tex, string name, int health, string weaponN, int weaponD, int speed)
    {
        playerTex = tex;
        playerName = name;
        playerHealth = health;
        weaponName = weaponN;
        weaponDamage = weaponD;
        playerSpeed = speed;
    }
}
