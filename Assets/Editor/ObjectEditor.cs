﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.ShortcutManagement;
using System;

public class ObjectEditor : EditorWindow
{
    public int status = 0;                             // Check if user wants to add a gameobject from the level editor window
    int selectedObj = 0;                                    // Index for Selected GameObject Tab in the window 
    string[] objects = new string[] { "Player", "Enemy Spawner", "Floor/Wall", "Interactive Objects", "Projectile" ,"Start & Exit" }; // List of gameObject types

    #region Player Variables

    public Texture playerTex;   // player sprite
    string playerName;          // player name
    int playerHealth = 0;       // player health
    string weaponName;          // player weapon name
    int weaponDamage = 0;       // player weapon damage
    int playerSpeed = 5;        // player speed

    #endregion

    #region Enemy Spawner Variables

    public Texture enemySpawnerTex1;    // Enemy Spawner sprite for stage 1 (Initial)
    public Texture enemySpawnerTex2;    // Enemy Spawner sprite for stage 2 (Weak)
    public Texture enemySpawnerTex3;    // Enemy Spawner sprite for stage 3 (Feeble)
    string enemySpawnerName;            // Spawner Name
    int spawnerHealth = 0;              // Spawner Health
    int spawnInterval = 0;              // Spawn Interval

    public Texture enemyTex;            // Enemy sprite
    string enemyName;                   // Enemy Name
    int enemyHealth = 0;                // Enemy Health
    int enemyDamage = 0;                // Enemy Damage
    string enemyWeaponName;             // Enemy weapon name
    int enemyWeaponDamage = 0;          // Enemy weapon damage
    int enemySpeed = 0;                 // Enemy speed

    #endregion

    #region Floor & Wall Variables

    public Texture f_w_Tex;
    string f_w_Name;
    int typeIndex = 0;
    string[] Types = new string[] { "Floor","Wall" };
    bool collidable = false;
    bool destructible = false;
    #endregion

    #region Interactive Objects Variables

    public Texture ioTex;        // Interactive Obj sprite
    string io_Name;              // Interactive Obj name    
    bool ioCollidable = false;   // is it collidable
    bool ioDestructible = false; // is it destructible

    #endregion

    #region Start & Exit Variables

    public Texture startTex;    // Start point sprite
    public Texture exitTex;     // End point sprite 

    #endregion

    #region Projectile Variables

    public Texture projTex;    // Texture for Projectile
    public string projName;    // Name of Projectile 
    public int projDamage;     // Damage of Projectile
    public int projSpeed;      // Speed of Projectile

    #endregion
    void OnEnable()
    {
        EditorWindow window = GetWindow<ObjectEditor>();
        window.minSize = new Vector2(900, 650);
        //window.maxSize = new Vector2(900, 650);
        window.titleContent = new GUIContent("GameObject Editor");
        window.Show();
    }

    void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        {
            // Tab of GameObject types

            selectedObj = GUILayout.Toolbar(selectedObj, objects);
            if (selectedObj == 0)
            {
                // Player Data Container

                #region Player Data
                GUILayout.BeginArea(new Rect(20, 10, 1150, 1150));
                {
                    EditorGUILayout.BeginVertical();
                    {
                        GUI.Box(new Rect(new Vector2(0, 50), new Vector2(850, 400)), "Player Data");

                        GUILayout.BeginArea(new Rect(new Vector2(Convert.ToSingle(this.position.width * 0.2), 130), new Vector2(Convert.ToSingle(this.position.width), Convert.ToSingle(this.position.height))));
                        {
                            EditorGUILayout.BeginHorizontal();
                            {
                                // Player Name

                                EditorGUILayout.LabelField("Name :", GUILayout.MaxWidth(150));
                                playerName = EditorGUILayout.TextField(playerName, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                // Player Health

                                EditorGUILayout.LabelField("Health :", GUILayout.MaxWidth(150));
                                playerHealth = EditorGUILayout.IntField(playerHealth, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                // Player Weapon Name

                                EditorGUILayout.LabelField("Weapon Name :", GUILayout.MaxWidth(150));
                                weaponName = EditorGUILayout.TextField(weaponName, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                // Player Weapon Damage

                                EditorGUILayout.LabelField("Weapon Damage :", GUILayout.MaxWidth(150));
                                weaponDamage = EditorGUILayout.IntField(weaponDamage, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                // Player Speed

                                EditorGUILayout.LabelField("Speed :", GUILayout.MaxWidth(150));
                                playerSpeed = EditorGUILayout.IntField(playerSpeed, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                // Player Sprite

                                playerTex = (Texture)EditorGUILayout.ObjectField("Player Sprite :", playerTex, typeof(Texture), true, GUILayout.MaxWidth(220));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                // On clicking Add Player button a message will be displayed

                                if (GUILayout.Button("Add Player", GUILayout.MaxWidth(100), GUILayout.MaxHeight(50)))
                                {
                                    //ScriptableObject player = new Player(playerTex,playerName,playerHealth,weaponName,weaponDamage,playerSpeed);
                                    Player asset = ScriptableObject.CreateInstance<Player>();
                                    asset = new Player(playerTex, playerName, playerHealth, weaponName, weaponDamage, playerSpeed);

                                    AssetDatabase.CreateAsset(asset, "Assets/" + playerName +  ".asset");
                                    AssetDatabase.SaveAssets();


                                    EditorUtility.DisplayDialog("GameObject Editor Notice", "Player data has been added !", "OK");
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        GUILayout.EndArea();
                    }
                    EditorGUILayout.EndVertical();
                }
                GUILayout.EndArea();
                #endregion
            }

            if (selectedObj == 1)
            {
                // Enemy Spawner Data Container

                #region Enemy Spawner Data
                GUILayout.BeginArea(new Rect(20, 10, 1150, 1150));
                {
                    EditorGUILayout.BeginVertical();
                    {
                        GUI.Box(new Rect(new Vector2(0, 50), new Vector2(850, 550)), "Enemy Spawner Data");

                        GUILayout.BeginArea(new Rect(new Vector2(Convert.ToSingle(this.position.width * 0.2), 130), new Vector2(Convert.ToSingle(this.position.width), Convert.ToSingle(this.position.height))));
                        {
                            // Enemy Spawner Name

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Name :", GUILayout.MaxWidth(150));
                                enemySpawnerName = EditorGUILayout.TextField(enemySpawnerName, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Enemy Spawner Health

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Health :", GUILayout.MaxWidth(150));
                                spawnerHealth = EditorGUILayout.IntField(spawnerHealth, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Enemy Spawner spawn interval

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Spawn Interval :", GUILayout.MaxWidth(150));
                                spawnInterval = EditorGUILayout.IntField(spawnInterval, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Enemy Spawner sprites

                            EditorGUILayout.BeginHorizontal();
                            {
                                enemySpawnerTex1 = (Texture)EditorGUILayout.ObjectField("Spawner Sprite :", enemySpawnerTex1, typeof(Texture), true, GUILayout.MaxWidth(220));
                                enemySpawnerTex2 = (Texture)EditorGUILayout.ObjectField("", enemySpawnerTex2, typeof(Texture), true, GUILayout.MaxWidth(120));
                                enemySpawnerTex3 = (Texture)EditorGUILayout.ObjectField("", enemySpawnerTex3, typeof(Texture), true, GUILayout.MaxWidth(120));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Seperator
                            EditorGUI.DrawRect(new Rect(0, 150, 560, 2), Color.black);
                            EditorGUILayout.Space();

                            // Enemy name 

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Enemy Name :", GUILayout.MaxWidth(150));
                                enemyName = EditorGUILayout.TextField(enemyName, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Enemy health

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Enemy Health :", GUILayout.MaxWidth(150));
                                enemyHealth = EditorGUILayout.IntField(enemyHealth, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            
                            // Enemy damage

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Enemy Damage :", GUILayout.MaxWidth(150));
                                enemyDamage = EditorGUILayout.IntField(enemyDamage, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Enemy Weapon name

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Enemy Weapon Name :", GUILayout.MaxWidth(150));
                                enemyWeaponName = EditorGUILayout.TextField(enemyWeaponName, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Enemy Weapon damage

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Enemy Weapon Damage :", GUILayout.MaxWidth(150));
                                enemyWeaponDamage = EditorGUILayout.IntField(enemyWeaponDamage, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Enemy Speed

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Enemy Speed :", GUILayout.MaxWidth(150));
                                enemySpeed = EditorGUILayout.IntField(enemySpeed, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Enemy sprite

                            EditorGUILayout.BeginHorizontal();
                            {
                                enemyTex = (Texture)EditorGUILayout.ObjectField("Enemy Sprite :", enemyTex, typeof(Texture), true, GUILayout.MaxWidth(220));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                // On clicking Add Enemy Spawner button a message will be displayed

                                if (GUILayout.Button("Add Enemy Spawner", GUILayout.MaxWidth(150), GUILayout.MaxHeight(50)))
                                {
                                    EnemySpawner asset = ScriptableObject.CreateInstance<EnemySpawner>();
                                    asset = new EnemySpawner(enemySpawnerTex1,enemySpawnerTex2,enemySpawnerTex3,enemySpawnerName,spawnerHealth,spawnInterval,enemyTex,enemyName,enemyHealth,enemyDamage,enemyWeaponName,enemyWeaponDamage,enemySpeed);

                                    AssetDatabase.CreateAsset(asset, "Assets/" + enemySpawnerName + ".asset");
                                    AssetDatabase.SaveAssets();

                                    EditorUtility.DisplayDialog("GameObject Editor Notice", "Enemy Spawner data has been added !", "OK");
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        GUILayout.EndArea();
                    }
                    EditorGUILayout.EndVertical();
                }
                GUILayout.EndArea();
                #endregion
            }

            if(selectedObj == 2)
            {
                // Floor and Wall Data container

                #region Floor & Wall Data
                GUILayout.BeginArea(new Rect(20, 10, 1150, 1150));
                {
                    EditorGUILayout.BeginVertical();
                    {
                        GUI.Box(new Rect(new Vector2(0, 50), new Vector2(850, 400)), "Floor/Wall Data");

                        GUILayout.BeginArea(new Rect(new Vector2(Convert.ToSingle(this.position.width * 0.2), 130), new Vector2(Convert.ToSingle(this.position.width), Convert.ToSingle(this.position.height))));
                        {
                            //Floor or Wall name

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Name :", GUILayout.MaxWidth(150));
                                f_w_Name = EditorGUILayout.TextField(f_w_Name, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Type (Floor or Wall)
                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Type :", GUILayout.MaxWidth(150));
                                typeIndex = EditorGUI.Popup(new Rect(160, 25, 220, 20),"",typeIndex,Types );
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Check if collidable
                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Collidable :", GUILayout.MaxWidth(150));
                                collidable = EditorGUILayout.Toggle(collidable);
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Check if destructible
                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Destructible :", GUILayout.MaxWidth(150));
                                destructible = EditorGUILayout.Toggle(destructible);
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Floor or Wall sprite
                            EditorGUILayout.BeginHorizontal();
                            {
                                f_w_Tex = (Texture)EditorGUILayout.ObjectField(Types[typeIndex] + " Sprite :", f_w_Tex, typeof(Texture), true, GUILayout.MaxWidth(220));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.Space();

                            // On clicking Add Enemy Spawner button a message will be displayed

                            EditorGUILayout.BeginHorizontal();
                            {
                                if (GUILayout.Button("Add " + Types[typeIndex], GUILayout.MaxWidth(100), GUILayout.MaxHeight(50)))
                                {
                                    Floor_Wall asset = ScriptableObject.CreateInstance<Floor_Wall>();
                                    asset = new Floor_Wall(f_w_Tex, f_w_Name,typeIndex, collidable,destructible);

                                    AssetDatabase.CreateAsset(asset, "Assets/" + f_w_Name + ".asset");
                                    AssetDatabase.SaveAssets();

                                    EditorUtility.DisplayDialog("GameObject Editor Notice", Types[typeIndex] + " data has been added !", "OK");
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        GUILayout.EndArea();
                    }
                    EditorGUILayout.EndVertical();
                }
                GUILayout.EndArea();
                #endregion
            }

            if(selectedObj == 3)
            {
                // Interactive Objects(IObj) Data Container

                #region Interactive Objects Data
                GUILayout.BeginArea(new Rect(20, 10, 1150, 1150));
                {
                    EditorGUILayout.BeginVertical();
                    {
                        GUI.Box(new Rect(new Vector2(0, 50), new Vector2(850, 400)), "Interactive Objects Data");

                        GUILayout.BeginArea(new Rect(new Vector2(Convert.ToSingle(this.position.width * 0.2), 130), new Vector2(Convert.ToSingle(this.position.width), Convert.ToSingle(this.position.height))));
                        {
                            // IObj Name

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Name :", GUILayout.MaxWidth(150));
                                io_Name = EditorGUILayout.TextField(io_Name, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Check if IObj is Collidable 

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Collidable :", GUILayout.MaxWidth(150));
                                ioCollidable = EditorGUILayout.Toggle(ioCollidable);
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Check if IObj is Destructible

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Destructible :", GUILayout.MaxWidth(150));
                                ioDestructible = EditorGUILayout.Toggle(ioDestructible);
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // IObj sprite

                            EditorGUILayout.BeginHorizontal();
                            {
                                ioTex = (Texture)EditorGUILayout.ObjectField("Interactive Obj Sprite :", ioTex, typeof(Texture), true, GUILayout.MaxWidth(220));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.Space();

                            // On clicking Add Interactive Object button a message will be displayed

                            EditorGUILayout.BeginHorizontal();
                            {
                                if (GUILayout.Button("Add Interactive Object", GUILayout.MaxWidth(190), GUILayout.MaxHeight(50)))
                                {
                                    InteractiveObjects asset = ScriptableObject.CreateInstance<InteractiveObjects>();
                                    asset = new InteractiveObjects(ioTex, io_Name,ioCollidable,ioDestructible);

                                    AssetDatabase.CreateAsset(asset, "Assets/" + io_Name + ".asset");
                                    AssetDatabase.SaveAssets();

                                    EditorUtility.DisplayDialog("GameObject Editor Notice", "Interactive Object data has been added !", "OK");
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        GUILayout.EndArea();
                    }
                    EditorGUILayout.EndVertical();
                }
                GUILayout.EndArea();
                #endregion
            }

            if (selectedObj == 4)
            {
                // Projectile Data Container

                #region Projectile Data
                GUILayout.BeginArea(new Rect(20, 10, 1150, 1150));
                {
                    EditorGUILayout.BeginVertical();
                    {
                        GUI.Box(new Rect(new Vector2(0, 50), new Vector2(850, 400)), "Start & Exit Data");

                        GUILayout.BeginArea(new Rect(new Vector2(Convert.ToSingle(this.position.width * 0.2), 130), new Vector2(Convert.ToSingle(this.position.width), Convert.ToSingle(this.position.height))));
                        {
                            // Projectile Name

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Name :", GUILayout.MaxWidth(150));
                                projName = EditorGUILayout.TextField(projName, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Projectile Damage

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Damage :", GUILayout.MaxWidth(150));
                                projDamage = EditorGUILayout.IntField(projDamage, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Projectile Speed

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("Speed :", GUILayout.MaxWidth(150));
                                projSpeed = EditorGUILayout.IntField(projSpeed, GUILayout.MaxWidth(400));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            // Projectile sprite

                            EditorGUILayout.BeginHorizontal();
                            {
                                projTex = (Texture)EditorGUILayout.ObjectField("Projectile Sprite :", projTex, typeof(Texture), true, GUILayout.MaxWidth(220));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();

                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                if (GUILayout.Button("Add Projectile", GUILayout.MaxWidth(190), GUILayout.MaxHeight(50)))
                                {
                                    Projectile asset = ScriptableObject.CreateInstance<Projectile>();
                                    asset = new Projectile(projTex, projName, projDamage, projSpeed);

                                    AssetDatabase.CreateAsset(asset, "Assets/" + projName + ".asset");
                                    AssetDatabase.SaveAssets();

                                    EditorUtility.DisplayDialog("GameObject Editor Notice", "Projectile data has been added !", "OK");
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        GUILayout.EndArea();
                    }
                    EditorGUILayout.EndVertical();
                }
                GUILayout.EndArea();
                #endregion
            }

            if (selectedObj == 5)
            {
                // Start and Exit Data Container

                #region Start & Exit Data
                GUILayout.BeginArea(new Rect(20, 10, 1150, 1150));
                {
                    EditorGUILayout.BeginVertical();
                    {
                        GUI.Box(new Rect(new Vector2(0, 50), new Vector2(850, 400)), "Start & Exit Data");

                        GUILayout.BeginArea(new Rect(new Vector2(Convert.ToSingle(this.position.width * 0.2), 130), new Vector2(Convert.ToSingle(this.position.width), Convert.ToSingle(this.position.height))));
                        {
                            // Start and Exit point sprite

                            EditorGUILayout.BeginHorizontal();
                            {
                                startTex = (Texture)EditorGUILayout.ObjectField("Start Point Sprite :", startTex, typeof(Texture), true, GUILayout.MaxWidth(220));
                                exitTex = (Texture)EditorGUILayout.ObjectField("Exit Point Sprite :", exitTex, typeof(Texture), true, GUILayout.MaxWidth(220));
                            }
                            EditorGUILayout.EndHorizontal();
                            EditorGUILayout.Space();
                            EditorGUILayout.Space();
                            EditorGUILayout.BeginHorizontal();
                            {
                                if (GUILayout.Button("Add Start & Exit", GUILayout.MaxWidth(190), GUILayout.MaxHeight(50)))
                                {
                                    Start_Exit asset = ScriptableObject.CreateInstance<Start_Exit>();
                                    asset = new Start_Exit(startTex, exitTex);

                                    AssetDatabase.CreateAsset(asset, "Assets/Start_Exit.asset");
                                    AssetDatabase.SaveAssets();

                                    EditorUtility.DisplayDialog("GameObject Editor Notice", "Start and Exit data has been added !", "OK");
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        GUILayout.EndArea();
                    }
                    EditorGUILayout.EndVertical();
                }
                GUILayout.EndArea();
                #endregion
            }
        }
        EditorGUILayout.EndVertical();
    }
}
