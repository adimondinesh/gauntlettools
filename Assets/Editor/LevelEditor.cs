﻿using UnityEngine;
using UnityEditor;
using UnityEditor.ShortcutManagement;
using UnityEngine.UIElements;
using System.Collections.Generic;
using UnityEditorInternal;
using System;
using SimpleJSON;
using System.IO;

class LevelEditor : EditorWindow
{
    ObjectEditor newWindow = null;
    public int layerId = 0;                                                         // id of selected layer
    public string[] layerList = new string[] { "1", "2", "3" };                      // List of layers

    int rowCount = 32;          // number of rows in level grid
    int columnCount = 32;       // number of columns in level grid
    int cellSize = 40;          // size of each size in level grid
    string levelName;           // name of level

    Dictionary<Vector2Int, Texture> layer1Data = new Dictionary<Vector2Int, Texture>();     // Scriptable object data to print on Layer 1
    Dictionary<Vector2Int, ScriptableObject> layer1Json = new Dictionary<Vector2Int, ScriptableObject>();     // Texture data to print on Layer 1
    Dictionary<Vector2Int, Texture> layer2Data = new Dictionary<Vector2Int, Texture>();     // Scriptable object data to print on Layer 2
    Dictionary<Vector2Int, ScriptableObject> layer2Json = new Dictionary<Vector2Int, ScriptableObject>();     // Texture data to print on Layer 2
    Dictionary<Vector2Int, Texture> layer3Data = new Dictionary<Vector2Int, Texture>();     // Scriptable object data to print on Layer 3
    Dictionary<Vector2Int, ScriptableObject> layer3Json = new Dictionary<Vector2Int, ScriptableObject>();     // Texture data to print on Layer 3
    Vector2 scrollPos;          // Scroll Position for Level Grid
    Vector2 scrollPos2;         // Scroll Position for Sprite List
    
    static List<ScriptableObject> assetList = new List<ScriptableObject>();     // A list to store all scriptable objects in the asset folder

    public Texture selectedTexture;             // The active texture that will be painted in the level grid
    public ScriptableObject selectedObject;     // The scriptable object of the active texture that will be painted in the level grid

    // open the window from the menu item Editor -> Level Editor
    [Shortcut("Refresh Editor", KeyCode.F5)]
    [MenuItem("Editor/Level Editor")]
    static void Init()
    {
        EditorWindow window = GetWindow<LevelEditor>();
        window.minSize = new Vector2(1500, 950);
        window.titleContent = new GUIContent("Gaunlet level Editor");
        window.Show();
        getAssets();       // method to retrieve all scriptable objects in the assetList
    }

    void OnGUI()
    {
        // Main Window Container
        GUILayout.BeginArea(new Rect(new Vector2(10, 10), new Vector2(1500, 950)));
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal();
                {
                    //New Level Button
                    if (GUILayout.Button("New Level"))
                    {
                        LevelEditor window = new LevelEditor();
                        window.minSize = new Vector2(1500, 1000);
                        window.titleContent = new GUIContent("Gaunlet level Editor");
                        window.Show();
                        this.Close();
                    }

                    //Edit Level Button
                    if (GUILayout.Button("Edit Level"))
                    {

                    }

                    //Add GameObject Button
                    if (GUILayout.Button("Add GameObject"))
                    {
                        newWindow = GetWindow<ObjectEditor>();
                        newWindow.Show();
                    }

                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginVertical();
                {
                    // Save Level Button
                    GUILayout.FlexibleSpace();
                    if (GUILayout.Button("Save Level", GUILayout.Height(50)))
                    {
                        string file = "[";
                        string path = Application.dataPath + "/Level.json";
                        string player = "";
                        int count = 0;

                        foreach (var position in layer1Json)
                        {
                            count++;
                            if((count)==layer1Json.Count)
                            {
                                player += JsonUtility.ToJson(position.Key) + JsonUtility.ToJson(position.Value);
                            }
                            else
                            {
                                player += JsonUtility.ToJson(position.Key) + JsonUtility.ToJson(position.Value) + ",";
                            }
                        }
                        file += player + "]";
                        File.WriteAllText(path, file);
                        Debug.Log(file);
                    }
                }
                EditorGUILayout.EndVertical();

                // Level Grid Container
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.BeginArea(new Rect(new Vector2(10, 70), new Vector2(1000, 800)));
                    {
                        GUILayout.BeginHorizontal();
                        {
                            scrollPos = GUILayout.BeginScrollView(scrollPos);
                            {
                                GUILayout.Label("", GUILayout.Width((columnCount * cellSize) + 50), GUILayout.Height((rowCount * cellSize) + 50));
                                
                                //Level Grid Box

                                Event evt = Event.current;
                                GUI.Box(new Rect(new Vector2(0, 0), new Vector2((cellSize * columnCount) + 20, (cellSize * rowCount) + 20)), "");
                                if (evt.type.Equals(EventType.MouseDown) && evt.button == 0)      //Check for mouse click event
                                {
                                    // Check the position of mouse click
                                    if (evt.mousePosition.x < (columnCount * cellSize) && evt.mousePosition.y < (rowCount * cellSize))
                                    {
                                        int rCell = (int)evt.mousePosition.x / cellSize;    // which row the user clicked in the level grid
                                        int cCell = (int)evt.mousePosition.y / cellSize;    // which column the user clicked in the level grid

                                        Vector2Int drawCell = new Vector2Int(rCell, cCell); // Vector to store the exact location of the box in the grid inside which the texture will be painted
                                        
                                        // Check if layer 1 is active and if the clicked position has a texture
                                        if (!layer1Data.ContainsKey(drawCell) && layerId == 0)
                                        {
                                            layer1Data.Add(drawCell, selectedTexture);  // If selected area is empty paint else remove
                                            layer1Json.Add(drawCell, selectedObject); // If area is painted store the scriptable object with position reference
                                        }
                                        else if(layerId == 0)
                                        {
                                            layer1Data.Remove(drawCell);   // Erase texture if already painted     
                                            layer1Json.Remove(drawCell);   // Remove the data of scriptable object associated with the erased texture
                                        }

                                        // Check if layer 2 is active and if the clicked position has a texture
                                        if (!layer2Data.ContainsKey(drawCell) && layerId == 1)
                                        {
                                            layer2Data.Add(drawCell, selectedTexture);  // If selected area is empty paint else remove
                                            layer2Json.Add(drawCell, selectedObject); // If area is painted store the scriptable object with position reference
                                        }
                                        else if (layerId == 1)
                                        {
                                            layer2Data.Remove(drawCell);
                                            layer2Json.Remove(drawCell);   // Remove the data of scriptable object associated with the erased texture
                                        }

                                        // Check if layer 3 is active and if the clicked position has a texture
                                        if (!layer3Data.ContainsKey(drawCell) && layerId == 2)
                                        {
                                            layer3Data.Add(drawCell, selectedTexture);  // If selected area is empty paint else remove
                                            layer3Json.Add(drawCell, selectedObject); // If area is painted store the scriptable object with position reference
                                        }
                                        else if (layerId == 2)
                                        {
                                            layer3Data.Remove(drawCell);
                                            layer3Json.Remove(drawCell);   // Remove the data of scriptable object associated with the erased texture
                                        }
                                    }
                                    Repaint();
                                }

                                foreach (var position in layer3Data)     //painting the selected cell on layer 3
                                {
                                    // checks if the painted texture is outside the scope of the grid and removes if it is in layer 3
                                    if (((position.Key.x) > (columnCount - 1)) || ((position.Key.y) > (rowCount - 1)))  
                                    {
                                        layer3Data.Remove(position.Key);
                                        layer3Json.Remove(position.Key); 
                                    }
                                    GUI.DrawTexture(new Rect(new Vector2((position.Key.x * cellSize) + 10, (position.Key.y * cellSize) + 10), new Vector2(cellSize, cellSize)), position.Value);
                                }

                                foreach (var position in layer2Data)     //painting the selected cell on layer 2
                                {
                                    // checks if the painted texture is outside the scope of the grid and removes if it is in layer 2
                                    if (((position.Key.x) > (columnCount - 1)) || ((position.Key.y) > (rowCount - 1)))
                                    {
                                        layer2Data.Remove(position.Key);
                                        layer2Json.Remove(position.Key);
                                        break;
                                    }
                                    GUI.DrawTexture(new Rect(new Vector2((position.Key.x * cellSize) + 10, (position.Key.y * cellSize) + 10), new Vector2(cellSize, cellSize)), position.Value);
                                }

                                foreach (var position in layer1Data)     //painting the selected cell on layer 1
                                {
                                    // checks if the painted texture is outside the scope of the grid and removes if it is in layer 1
                                    if (((position.Key.x) > (columnCount - 1)) || ((position.Key.y) > (rowCount - 1)))
                                    {
                                        layer1Data.Remove(position.Key);
                                        layer1Json.Remove(position.Key);
                                        break;
                                    }
                                    GUI.DrawTexture(new Rect(new Vector2((position.Key.x * cellSize) + 10, (position.Key.y * cellSize) + 10), new Vector2(cellSize, cellSize)), position.Value);
                                }

                                //Level Grid
                                for (int r = 0; r < rowCount + 1; r++)
                                {
                                    EditorGUI.DrawRect(new Rect(new Vector2(10, (r * cellSize) + 10), new Vector2(columnCount * cellSize, 1)), Color.black);
                                }

                                for (int t = 0; t < columnCount + 1; t++)
                                {
                                    EditorGUI.DrawRect(new Rect(new Vector2((t * cellSize) + 10, 10), new Vector2(1, rowCount * cellSize)), Color.black);
                                }
                            }
                            GUILayout.EndScrollView();
                        }
                        GUILayout.EndHorizontal();
                    }
                    GUILayout.EndArea();


                    // Layer Grid Container
                    GUILayout.BeginArea(new Rect(new Vector2(1100, 0), new Vector2(1000, 1000)));
                    {
                        GUI.Box(new Rect(new Vector2(0, 70), new Vector2(170, 100)), "Layers");

                        EditorGUILayout.BeginHorizontal();
                        layerId = GUI.SelectionGrid(new Rect(10, 100, 150, 50), layerId, layerList, 3);     // Layer Grid
                        EditorGUILayout.EndHorizontal();

                    }
                    GUILayout.EndArea();

                    // Object Grid Container
                    GUILayout.BeginArea(new Rect(new Vector2(1100, 200), new Vector2(300, 650)));
                    {
                        GUILayout.BeginVertical();
                        {
                            int i = -90;    // An offset space between each object in the vertical direction
                            GUI.Box(new Rect(new Vector2(0, 0), new Vector2(240, 650)), "Sprites");
                            scrollPos2 = GUILayout.BeginScrollView(scrollPos2);
                            {
                                GUILayout.Label("", GUILayout.Width(250), GUILayout.Height(assetList.Count * 300)); // Determines the scroll area
                                GUILayout.BeginVertical();
                                foreach (var scriptableObj in assetList)    // Iterate through the list of scriptable objects in the assetList
                                {
                                    // Check type of scriptable object and make a button which displays and activates the texture that needs to be painted on the grid

                                    if (scriptableObj.GetType().Equals(typeof(Player)))
                                    {
                                        Player player = (Player)scriptableObj;
                                        if (GUI.Button(new Rect(60, i + 150, 120, 120), player.playerTex))
                                        {
                                            selectedObject = scriptableObj;
                                            selectedTexture = player.playerTex;
                                        }
                                        i += 150;
                                    }

                                    else if (scriptableObj.GetType().Equals(typeof(Start_Exit)))
                                    {
                                        Start_Exit s_e = (Start_Exit)scriptableObj;
                                        if (GUI.Button(new Rect(60, i + 150, 120, 120), s_e.startTex))
                                        {
                                            selectedObject = scriptableObj;
                                            selectedTexture = s_e.startTex;
                                        }
                                        i += 150;

                                        if (GUI.Button(new Rect(60, i + 150, 120, 120), s_e.exitTex))
                                        {
                                            selectedObject = scriptableObj;
                                            selectedTexture = s_e.exitTex;
                                        }
                                        i += 150;
                                    }

                                    else if (scriptableObj.GetType().Equals(typeof(EnemySpawner)))
                                    {
                                        EnemySpawner spawner = (EnemySpawner)scriptableObj;
                                        if (GUI.Button(new Rect(60, i + 150, 120, 120), spawner.enemySpawnerTex1))
                                        {
                                            selectedObject = scriptableObj;
                                            selectedTexture = spawner.enemySpawnerTex1;
                                        }
                                        i += 150;
                                    }

                                    else if (scriptableObj.GetType().Equals(typeof(Floor_Wall)))
                                    {
                                        Floor_Wall f_w = (Floor_Wall)scriptableObj;
                                        if (GUI.Button(new Rect(60, i + 150, 120, 120), f_w.f_w_Tex))
                                        {
                                            selectedObject = scriptableObj;
                                            selectedTexture = f_w.f_w_Tex;
                                        }
                                        i += 150;
                                    }

                                    else if (scriptableObj.GetType().Equals(typeof(InteractiveObjects)))
                                    {
                                        InteractiveObjects io = (InteractiveObjects)scriptableObj;
                                        if (GUI.Button(new Rect(60, i + 150, 120, 120), io.ioTex))
                                        {
                                            selectedObject = scriptableObj;
                                            selectedTexture = io.ioTex;
                                        }
                                        i += 150;
                                    }
                                }
                                GUILayout.EndVertical();
                            }
                            GUILayout.EndScrollView();

                        }
                        GUILayout.EndVertical();
                    }
                    GUILayout.EndArea();
                }
                EditorGUILayout.EndHorizontal();

                //Level Details Container
                GUILayout.BeginArea(new Rect(new Vector2(0, 30), new Vector2(800, 800)));
                {
                    // Level Name
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField("Level Name :", GUILayout.MaxWidth(100));
                        levelName = EditorGUILayout.TextField(levelName, GUILayout.MaxWidth(400));
                    }
                    EditorGUILayout.EndHorizontal();

                    // Rows and Columns in Level Grid
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField("Rows :", GUILayout.MaxWidth(50));
                        rowCount = Convert.ToInt32(EditorGUILayout.Slider(rowCount, 32, 64, GUILayout.MaxWidth(200)));
                        EditorGUILayout.LabelField("Columns :", GUILayout.MaxWidth(50));
                        columnCount = Convert.ToInt32(EditorGUILayout.Slider(columnCount, 32, 64, GUILayout.MaxWidth(200)));
                    }
                    EditorGUILayout.EndHorizontal();
                }
                GUILayout.EndArea();
            }
            EditorGUILayout.EndVertical();
        }
        GUILayout.EndArea();
    }

    static void getAssets()
    {
        // Find scriptable objects and store them in a string array.
        string[] stringtoarray = AssetDatabase.FindAssets("t:ScriptableObject", new[] { "Assets" });
        assetList.Clear();

        // Get guids of each scriptable object from the string array
        foreach (var guid1 in stringtoarray)
        {
            // store the path of the scriptable object from its guid and add in the asset list
            string test = AssetDatabase.GUIDToAssetPath(guid1);     
            assetList.Add((ScriptableObject)AssetDatabase.LoadMainAssetAtPath(test));
        }
    }
}